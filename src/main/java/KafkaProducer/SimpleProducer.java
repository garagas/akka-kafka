package KafkaProducer;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

/**
 *
 */
public class SimpleProducer {
    //private final static String BOOTSTRAP_SERVERS = "34.241.217.181:9092";
    private final static String BOOTSTRAP_SERVERS = "localhost:9092";
    // private final static String BOOTSTRAP_SERVERS = "172.31.6.80:9092";
    private final static String DESTINATION_TOPIC = "example";
    private final static int MESSAGE_NUMBER = 1000000;

    public static void main(String args[]){


        //create producer
        Producer<Integer, String> producer = createProducer();

        // create append message
        StringBuffer stringBuffer = new StringBuffer();
        for (int j=0; j< 400; j++)
        {
            stringBuffer.append("a");
        }

        //send messages
        for(int i = 0; i < MESSAGE_NUMBER; i++) {
            ProducerRecord producerRecord = new ProducerRecord<>(DESTINATION_TOPIC,
                    i,
                    "Test Message #" + i+ stringBuffer.toString());
            producer.send(producerRecord);
            System.out.println(i);
        }
        producer.flush();

        //close producer
        producer.close();
    }


    /**
     * Method that create a kafka producer
     * @return
     */
    private static KafkaProducer<Integer, String> createProducer(){
        //properties for producer
        Properties props = new Properties();
        props.put("bootstrap.servers", BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return new KafkaProducer<>(props);
    }

}
