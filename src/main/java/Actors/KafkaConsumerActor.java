package Actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.*;

/**
 * Actor used to retrieve messages from the Kafka Topic, is the main entry point of the monolithic block
 */
public class KafkaConsumerActor extends AbstractActor{

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    /**
     * Constant values
     */
    private final static String BOOTSTRAP_SERVERS = "localhost:9092";
    private final static String GROUP_ID = "test";
    private final static String TOPIC_NAME = "example";
    private final static Duration RESCHEDULED_KAFKA_POLL_DELAY = Duration.ofMillis(3000);
    /**
     * Kafka consumer used to poll messages from Kafka
     */
    private KafkaConsumer<String, String> kafkaConsumer;
    /**
     * Ref to the Akka actor routing
     */
    private ActorRef routerActor;
    /**
     * Tells how many partitions have been retrieve from the poll call. It then identifies how many worker actors
     * will be working. One partition goes only to one worker. How many request are sent to Workers Actor
     */
    private int numberOfRequestToComplete;
    /**
     * How many work-partition request have been completed by workers Actor per Kafka Poll
     */
    private int numberOfRequestCompleted;

    /**
     * Create Props for an actor of this type.
     * @param router
     * @return
     */
    public static Props props(ActorRef router){
       return Props.create(KafkaConsumerActor.class, ()-> new KafkaConsumerActor(router));
    }

    /**
     * constructor
     * @param routerActor
     */
    public KafkaConsumerActor(ActorRef routerActor) {
        this.routerActor = routerActor;
        this.numberOfRequestToComplete = 0;
        this.numberOfRequestCompleted = 0;
    }


    /**
     * Class representing the message sent by this actor to the routing actor
     */
   public static final class KafkaMessage{
       final String topicName;
       final int partitionNumber;
       final Long offsetNumber;
       final String content;
       final int recordsInPartition;
       final int positionInBatch;

       public KafkaMessage(String topicName, int partitionNumber, Long offsetNumber, String content,
                           int recordsInPartition, int positionInBatch) {
           this.topicName = topicName;
           this.partitionNumber = partitionNumber;
           this.offsetNumber = offsetNumber;
           this.content = content;
           this.recordsInPartition = recordsInPartition;
           this.positionInBatch = positionInBatch;
       }

        public String getTopicName() {
            return topicName;
        }

        public int getPartitionNumber() {
            return partitionNumber;
        }

        public String getContent() {
            return content;
        }
    }


    @Override
    public void preStart() throws Exception {
        super.preStart();
        log.info("Kafka consumer actor started");
        // creating the kafka consumer, subscribing and assigning the listener
        kafkaConsumer = new KafkaConsumer<>(buildConsumerProperties());
        RebalanceListener rebalanceListener = new RebalanceListener(kafkaConsumer);
        kafkaConsumer.subscribe(Arrays.asList(TOPIC_NAME), rebalanceListener);
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
        log.info("Kafka consumer actor stopped");
        // stopping the kafka consumer
        kafkaConsumer.close();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .matchEquals("start",
                        message ->{
                            consumeKafkaMessages();
                        })
                .matchEquals("consumeNewMessages",
                        message ->{
                            this.numberOfRequestCompleted++;
                            if(this.numberOfRequestCompleted == numberOfRequestToComplete)
                            {
                                log.info("All the worker have completed their job, requesting new messages "+
                                        " (received from {}) " +
                                        "Status: request sent {} - request completed {}", getSender(),
                                        this.numberOfRequestToComplete,
                                        this.numberOfRequestCompleted);
                                // resetting the number of completed request for the new Kafka consumer poll
                                this.numberOfRequestCompleted = 0;
                                getSelf().tell("start", getSelf());
                            }
                            else
                                log.info("Not all workers have completed their jobs, waiting for more responses"+
                                                " (received from {}) " +
                                        "Status: request sent {} - request completed {}", getSender(),
                                        this.numberOfRequestToComplete,
                                        this.numberOfRequestCompleted);
                        })
                .match(WorkerActor.committingOffsetMessage.class,
                        message ->{
                            log.info("Commit message received from {} : partition {}, offset {}",
                                    getSender(),
                                    message.partitionNumber,
                                    message.offsetNumber);
                            TopicPartition topicPartition = new TopicPartition(message.topicName,
                                    message.partitionNumber);
                            kafkaConsumer.commitSync(Collections.singletonMap(topicPartition,
                                    new OffsetAndMetadata(message.offsetNumber)));
                        })
                .build();
    }

    /**
     * Start consuming messages from Kafka, read and send them directly to the router Actor
     */
    private void consumeKafkaMessages(){
        log.info("----------- New Poll Request from Kafka Consumer ------------------");
        ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(1000));
        Set<TopicPartition> partitionsInPoll = records.partitions();
        log.info("Poll request topic and partition {}", partitionsInPoll);
        log.info("Received {} messages from Kafka", records.count());
        this.numberOfRequestToComplete = partitionsInPoll.size();
        // if data can be retrieved from Kafka
        if(!records.isEmpty()){
            for (TopicPartition partition : partitionsInPoll) {
                List<ConsumerRecord<String, String>> partitionRecords = records.records(partition);
                log.info("Message read from topic {}, offset start {} - offset end {} ", partition.toString(),
                        partitionRecords.get(0).offset(),
                        partitionRecords.get(partitionRecords.size()-1).offset());
                int counter = 1;
                int numberOfRecordsInPartition = partitionRecords.size();
                for (ConsumerRecord<String, String> record: partitionRecords) {
                    KafkaMessage message = new KafkaMessage(record.topic(),
                            record.partition(),
                            record.offset(),
                            record.offset() + record.value(),
                            numberOfRecordsInPartition,
                            counter);
                    // send the message to the router
                    routerActor.tell(message, getSelf());
                    counter ++;
                }
            }
        }
        else{
            log.info("Empty poll from Kafka - Scheduling a poll");
            // schedule a future kafka poll after some time
            getContext().getSystem().scheduler().scheduleOnce(RESCHEDULED_KAFKA_POLL_DELAY,
                    () -> {
                        getSelf().tell("start", getSelf());
                    }, getContext().getDispatcher());
        }
    }

    /**
     * Method used to set all the properties for the Kafka Consumer
     * @return the properties for building the Kafka Consumer
     */
    private Properties buildConsumerProperties(){
        Properties props = new Properties();
        props.put("bootstrap.servers", BOOTSTRAP_SERVERS);
        props.put("group.id", GROUP_ID);
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("enable.auto.commit","false");
        props.put("auto.offset.reset","earliest");
        return props;
    }
}
