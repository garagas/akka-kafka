package Actors;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;

/**
 * Class used to take and write to disk the messages received from the router actor
 */
public class WorkerActor extends AbstractActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    /**
     * Maximum number of messages withheld by the Worker before writing it to the filesystem
     */
    private final static int MAX_BUFFER_DIMENSION = 1000;
    /**
     * Buffer used to hold the messages received by the Worker
     */
    private ArrayList<KafkaConsumerActor.KafkaMessage> messageBuffer;

    /**
     * Message used to commit the offsets to Kafka
     */
    public static final class committingOffsetMessage{
        final String topicName;
        final Long offsetNumber;
        final int partitionNumber;

        public committingOffsetMessage(String topicName, Long offsetNumber, int partitionNumber) {
            this.topicName = topicName;
            this.offsetNumber = offsetNumber;
            this.partitionNumber = partitionNumber;
        }
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();
        log.info("Worker started");
        // initialize the kafka messages buffer
        messageBuffer = new ArrayList<>();
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
        log.info("Worker stopped");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(KafkaConsumerActor.KafkaMessage.class,
                        message -> {
                            //This check is performed since when partition rebalancing happens, all the partitions
                            // are revoked from the workers. The kafka consumer will then restart from the last commited
                            // offset. Some messages in the buffer may have bigger offset than the ones read from the
                            // current poll. This happens when there are some messages in the buffer during the
                            // rebalancing procedure.
                            if(messageBuffer.isEmpty() ||
                                    message.offsetNumber > messageBuffer.get(messageBuffer.size()-1).offsetNumber) {
                                // adding message to the buffer
                                messageBuffer.add(message);
                            }
                            else{
//                                log.info("Worker not added the message to the buffer, with offset {}, " +
//                                        "buffersize {}", message.offsetNumber, messageBuffer.size());
//                                if(!messageBuffer.isEmpty())
//                                    log.info("Worker not processed the message, with offset {}, " +
//                                                    "buffersize {}, startoffset {}, lastoffset {}", message.offsetNumber,
//                                            messageBuffer.size(),
//                                            messageBuffer.get(0).offsetNumber,
//                                            messageBuffer.get(messageBuffer.size()-1).offsetNumber);
                            }
                            // need to write it to disk and send a commit message if buffer is full
                            if(messageBuffer.size() == MAX_BUFFER_DIMENSION){
                                log.info("Buffer is full, starting the writing procedure on disk");
                                String topicName = messageBuffer.get(messageBuffer.size()-1).topicName;
                                int partitionNumber = messageBuffer.get(messageBuffer.size()-1).partitionNumber;
                                Long offsetEnd = messageBuffer.get(messageBuffer.size()-1).offsetNumber;
                                Long offsetStart = messageBuffer.get(0).offsetNumber;
                                // writing the buffer to disk
                                boolean writtenToDisk = startWritingBufferToDisk(topicName,
                                        partitionNumber,
                                        offsetStart,
                                        offsetEnd);
                                // can happen that the worker won't write to disk, when some messages in the buffer
                                // have already been committed by other block of workers
                                if(writtenToDisk) {
                                    KafkaConsumerActor.KafkaMessage lastMessage = messageBuffer.get(messageBuffer.size()-1);
                                    log.info("Sending the committing offset message to Kafka Consumer");
                                    // sending the commit message to the Kafka Consumer, the commit offset should point to
                                    // the next record
                                    if(!String.valueOf(lastMessage.offsetNumber).endsWith("999"))
                                        log.info("Commit message didn't end with 999");
                                    getSender().tell(new committingOffsetMessage(
                                            lastMessage.getTopicName(),
                                            lastMessage.offsetNumber + 1,
                                            lastMessage.partitionNumber), getSelf());
                                    // clear the buffer
                                    messageBuffer.clear();
                                }
                            }
                            // at the end check if it was the last message for the current poll batch
                            if(message.positionInBatch == message.recordsInPartition)
                            {
                                log.info("Worker {} finished processing messages" +
                                                ", requesting new messages to Kafka, current buffer size {}",
                                        getSelf(),
                                        messageBuffer.size());
                                getSender().tell("consumeNewMessages", getSelf());
                            }

                        })
                .build();
    }

    /**
     * Write the messages inside the buffer to Disk. The parameters are used to set the filename and folders.
     * All sort of checks will be performed by this method.
     * @param topicName name of the topic from where the messages are from
     * @param partitionNumber partition number from where the message are from
     * @param offsetStart
     * @param offsetEnd
     * @return true if the files are effectively written to disk. Can happen that the buffer will not be written since
     *          some of the messages have already been written by other blocks (return false)
     */
    private boolean startWritingBufferToDisk(String topicName, int partitionNumber, Long offsetStart, Long offsetEnd){
        try {
            // folder where the files of this particular worker are held
            File directory = new File("files/"+topicName+"_"+partitionNumber);
            // if it is the first time reading this topic/partition, create the new folder
            if(!directory.exists())
                directory.mkdirs();
            // find the last modified file inside the topic - partition folder, can be empty
            Optional<Path> lastModifiedFile = findLastModifiedFile(directory);
            Optional<Integer> lastOffsetWrittenToDisk;
            // there is a file in the folder topic - partition
            if(lastModifiedFile.isPresent())
            {
                // extract the last offset written
                lastOffsetWrittenToDisk = Optional.of(Integer.parseInt(lastModifiedFile.get().getFileName().toString()
                        .split("\\.")[0].split("_")[1]));

                // if is returned true, then something has been removed
                if(!performBufferCleaning(lastOffsetWrittenToDisk.get())){
                    File file = new File(directory, offsetStart + "_" + offsetEnd + ".txt");
                    BuildFileWriterAndWriteBuffer(file);
                    log.info("Buffer written to disk");
                    return true;
                }
            }
            // it is the first time reading messages from that partition
            else {
                File file = new File(directory, offsetStart + "_" + offsetEnd+".txt");
                BuildFileWriterAndWriteBuffer(file);
                log.info("Buffer written to disk");
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Buffer hasn't been written to disk, some messages have been removed");
        return false;
    }

    /**
     * Create the file writer and write to disk the message buffer
     * @param file
     * @throws IOException
     */
    private void BuildFileWriterAndWriteBuffer(File file) throws IOException {
        // new file that will be written by the Worker
        FileWriter writer = new FileWriter(file);
        for (KafkaConsumerActor.KafkaMessage msg : messageBuffer) {
            writer.write(msg.getContent());
            writer.write("\n");
        }
        writer.close();
    }

    /**
     * Find the last modified file inside a folder
     * @param directory
     * @return
     * @throws IOException
     */
    private Optional<Path> findLastModifiedFile(File directory) throws IOException {
        Path dir = directory.toPath();  // specify your directory
        Optional<Path> lastFilePath = Files.list(dir)    // here we get the stream with full directory listing
                .filter(f -> !Files.isDirectory(f))  // exclude subdirectories from listing
                .max(Comparator.comparingLong(f -> f.toFile().lastModified()));  // finally get the last file using simple comparator by lastModified field

        return lastFilePath;
    }


    /**
     * Method used to verify if the buffer contains old messages, written by others blocks of Workers-Router-Consumer
     * @param lastOffsetWritten
     * @return true if at least one element has been removed from the buffer, false otherwise
     */
    private boolean performBufferCleaning(long lastOffsetWritten){
        boolean removedElements = false;
        int counter = 0;
        Iterator<KafkaConsumerActor.KafkaMessage> messageBufferIterator = messageBuffer.iterator();
        while (messageBufferIterator.hasNext()){
            KafkaConsumerActor.KafkaMessage currentMessage = messageBufferIterator.next();
             // this buffer cleaning is performed when a kafka consumer with all the workers leaves the group.
            // there could be some inconsistency inside the buffer with older messages
            if(currentMessage.offsetNumber > lastOffsetWritten){
                break;
            }
            else
            {
//                log.info("Removed an element from the buffer from topic {} partition {}, offset {} ",
//                        currentMessage.topicName,
//                        currentMessage.partitionNumber,
//                        currentMessage.offsetNumber);
                removedElements = true;
                counter ++;
                messageBufferIterator.remove();
            }
        }
        log.info("Buffer cleaning procedure has been performed, removed elements: {}. Number of removed " +
                        "elements {} (last offset found {}, buffer size {})",
                removedElements,
                counter,
                lastOffsetWritten,
                messageBuffer.size());

        return removedElements;
    }
}
