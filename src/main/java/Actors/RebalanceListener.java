package Actors;

import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.util.Collection;

public class RebalanceListener implements ConsumerRebalanceListener {
    // this can be used to commit some offset
    private KafkaConsumer consumer;

    public RebalanceListener(KafkaConsumer con){
        this.consumer=con;
    }

    public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
        System.out.println("---------------------------------");
        System.out.print("Following Partitions Assigned ....");
        for(TopicPartition partition: partitions)
            System.out.print(partition.partition()+",");
        System.out.println(" ");
        System.out.println("---------------------------------");
    }

    public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
        System.out.println("---------------------------------");
        System.out.print("Following Partitions Revoked ....");
        for(TopicPartition partition: partitions)
            System.out.print(partition.partition()+",");
        System.out.println(" ");
        System.out.println("---------------------------------");

    }
}