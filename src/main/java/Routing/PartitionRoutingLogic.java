package Routing;

import Actors.KafkaConsumerActor;
import akka.routing.Routee;
import scala.collection.immutable.IndexedSeq;

public class PartitionRoutingLogic implements akka.routing.RoutingLogic {

    @Override
    public Routee select(Object message, IndexedSeq<Routee> routees) {
        KafkaConsumerActor.KafkaMessage kafkaMessage = (KafkaConsumerActor.KafkaMessage) message;
        return routees.apply(kafkaMessage.getPartitionNumber());
    }
}
