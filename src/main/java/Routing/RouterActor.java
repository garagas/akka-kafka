package Routing;

import Actors.KafkaConsumerActor;
import Actors.WorkerActor;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.ActorRefRoutee;
import akka.routing.Routee;
import akka.routing.Router;
import javafx.concurrent.Worker;

import java.util.ArrayList;
import java.util.List;

public class RouterActor extends AbstractActor{

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    private Router router;
    /**
     * Number of worker routees that the router has to manage and forward message to
     */
    private  int numberOfRoutees;
    /**
     * Variable to hold the routees
     */
    List<Routee> routees = new ArrayList<>();


    public RouterActor(int numberOfRoutees) {
        this.numberOfRoutees = numberOfRoutees;
    }

    public static Props props(int numberOfRoutees){
        return Props.create(RouterActor.class, () -> new RouterActor(numberOfRoutees));
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();
        log.info("Router Actor started");
        for (int i = 0; i < numberOfRoutees; i++) {
            ActorRef r = getContext().actorOf(Props.create(WorkerActor.class));
            getContext().watch(r);
            routees.add(new ActorRefRoutee(r));
        }
        router = new Router(new PartitionRoutingLogic(), routees);
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
        log.info("Router Actor stopped");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(KafkaConsumerActor.KafkaMessage.class,
                        message -> {
                            router.route(message, getSender());
                        })
                .match(
                        Terminated.class,
                        message -> {
                            //TODO: per mantenere sempre un ordinamento preciso è meglio usare una hashmap
                            router = router.removeRoutee(message.actor());
                            ActorRef r = getContext().actorOf(Props.create(Worker.class));
                            getContext().watch(r);
                            router = router.addRoutee(new ActorRefRoutee(r));
                        })
                .build();
    }

}

