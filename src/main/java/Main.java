import Actors.KafkaConsumerActor;
import Routing.RouterActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class Main {

    private final static int NUMBER_OF_WORKER_ROUTEES = 10;

    public static void main(String[] args){

        ActorSystem system = ActorSystem.create("system");

        // This three lines identifies a single block composed of
        // 1-actor for reading data from Kafka
        // 1-actor for dispatching/routing messages to workers
        // n-actors for writing messages to disk (1 for each kafka topic partition)
        ActorRef router = system.actorOf(Props.create(RouterActor.class, NUMBER_OF_WORKER_ROUTEES), "router");
        ActorRef kafkaConsumerActorRef = system.actorOf(KafkaConsumerActor.props(router));
        // start consuming new messages
        kafkaConsumerActorRef.tell("start", ActorRef.noSender());



    }
}
